using CliWrap;
using System.IO;
using System.Text;

namespace postgenerator.Util{
	static class ConsoleCommands{
		static StringBuilder stdOut = new StringBuilder();
		
		public static string GetCmdOutputStream(){
			return stdOut.ToString() ?? "";
		}

		public static async void FetchPosts(string remotePath){
			var mkdir = await Cli.Wrap("mkdir")
				.WithArguments("/tmp/postgen")
				.WithValidation(CommandResultValidation.None)
				.ExecuteAsync();
			var result = await Cli.Wrap("rsync")
				.WithArguments(remotePath + "/posts.py" + " /tmp/postgen")
				.WithStandardOutputPipe(PipeTarget.ToStringBuilder(stdOut))
				.ExecuteAsync();
		}
	}
}
