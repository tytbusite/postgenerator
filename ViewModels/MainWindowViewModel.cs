﻿using System;
using System.Collections.Generic;
using System.Text;
using Avalonia;

namespace postgenerator.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
	    public string TextBoxContent{ get; set; } = "";

	    public bool LeftPaneOpen{ get; set; } = true;

	    public void OpenLeftPane(){ LeftPaneOpen = true; }

	    public void CloseLeftPane() { LeftPaneOpen = false; }

	    public void FetchPosts() => Util.ConsoleCommands.FetchPosts(TextBoxContent);
    }
}
